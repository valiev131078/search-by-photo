from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from web.views import registration_view, auth_view, logout_view, main_view, image_create

urlpatterns = [
    path('', main_view, name="main"),
    path('registration/', registration_view, name="registration"),
    path('auth/', auth_view, name="auth"),
    path('logout_view/', logout_view, name="logout"),
    path('admin/', admin.site.urls),
    path('image/', image_create, name="image"),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
